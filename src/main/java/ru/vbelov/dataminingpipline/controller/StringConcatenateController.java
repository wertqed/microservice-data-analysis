package ru.vbelov.dataminingpipline.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vbelov.dataminingpipline.dto.InputDTO;
import ru.vbelov.dataminingpipline.dto.OutputDTO;
import ru.vbelov.dataminingpipline.dto.concatenate.StringInputDTO;

@RestController
@RequestMapping(value = "strings")
public class StringConcatenateController {

    @PostMapping("concatenate")
    public OutputDTO concatenateString(@RequestBody StringInputDTO input) throws InterruptedException {
//        Thread.sleep(5555);
        OutputDTO out = new OutputDTO();
        out.setOutput((input.getInput() != null ? input.getInput() : input.getStartStr()) + input.getValue());
        return out;
    }
}
