package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vbelov.dataminingpipline.model.FullProjectPipeline;
import ru.vbelov.dataminingpipline.model.ProjectPipeline;

import java.util.List;

@Repository
public interface ProjectPipelineRepository extends JpaRepository<ProjectPipeline, Long> {

    List<ProjectPipeline> findProjectPipelineByProjectId(Long projectId);

    void deleteAllByProjectId(Long projectId);

    /**
     * Удаляем все шаги, где id не из списка в разрезе проекта
     *
     * @param ids
     */
    void deleteAllByIdNotInAndProjectIdEquals(List<Long> ids, Long id);
}
