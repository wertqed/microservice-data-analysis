package ru.vbelov.dataminingpipline.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vbelov.dataminingpipline.dto.PipelineServiceDTO;
import ru.vbelov.dataminingpipline.model.PipelineServiceEntity;
import ru.vbelov.dataminingpipline.repository.PipelineServicesRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class PipelineServiceImpl implements PipelineService {
    private final Logger LOGGER = LoggerFactory.getLogger(PipelineServiceImpl.class);

    private final PipelineServicesRepository pipelineServicesRepository;

    @Autowired
    public PipelineServiceImpl(PipelineServicesRepository pipelineServicesRepository) {
        this.pipelineServicesRepository = pipelineServicesRepository;
    }

    @Override
    public void savePipelineService(PipelineServiceDTO pipelineServiceDTO) {
        LOGGER.debug("Saving new piplineservice = {}", pipelineServiceDTO);
        PipelineServiceEntity entity = new PipelineServiceEntity();
        BeanUtils.copyProperties(pipelineServiceDTO, entity);
        pipelineServicesRepository.save(entity);
    }

    @Override
    public List<PipelineServiceDTO> listPipelineServices() {
        LOGGER.debug("Loading pipelineservices list");
        List<PipelineServiceDTO> pipelineServiceDTOS = new ArrayList<>();
        pipelineServicesRepository.findAll().forEach(entity->{
            PipelineServiceDTO dto = new PipelineServiceDTO();
            BeanUtils.copyProperties(entity, dto);
            pipelineServiceDTOS.add(dto);
        });
        return pipelineServiceDTOS;
    }

    @Override
    public void deletePipelineService(Long id) {
        LOGGER.debug("Delete pipelineservice by id ={}", id);
        pipelineServicesRepository.deleteById(id);
    }
}
