package ru.vbelov.dataminingpipline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataminingPiplineApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataminingPiplineApplication.class, args);
    }

}
