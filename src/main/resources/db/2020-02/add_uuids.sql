alter table project_pipeline
    drop column pipe_ord;

alter table project_pipeline
    add column uuid uuid;

drop table pipeline_links;

create table pipeline_links
(
    id               serial primary key,
    name             character varying(250),
    project_id       integer references project (id),
    source_node_uuid uuid,
    target_node_uuid uuid
);