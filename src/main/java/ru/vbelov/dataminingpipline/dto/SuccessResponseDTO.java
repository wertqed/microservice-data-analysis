package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

@Data
public class SuccessResponseDTO {
    private ResponseStatus responseStatus;
    private String message;

    public SuccessResponseDTO(){}

    public SuccessResponseDTO(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
