package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vbelov.dataminingpipline.model.DataType;

public interface DataTypeRepository extends JpaRepository<DataType, Long> {
}
