package ru.vbelov.dataminingpipline.service;

import ru.vbelov.dataminingpipline.dto.ProjectDTO;
import ru.vbelov.dataminingpipline.dto.ProjectPipelineDTO;
import ru.vbelov.dataminingpipline.model.FullProjectPipeline;

import java.util.List;
import java.util.UUID;

public interface ProjectService {
    void saveProject(ProjectDTO projectDTO);
    List<ProjectDTO> listPrjects();
    void deleteProject(Long id);
    ProjectPipelineDTO getProjectPipeline(Long projectId);
    void savePipeline(ProjectPipelineDTO pipelineDTOS);
    FullProjectPipeline getProjecyStepResult(UUID stepUuid);
}
