alter table pipeline_service
    add column group_code character varying(100);

insert into service_group_type(name, code)
select 'Загрузка данных', 'DATALOADER';

insert into service_group_type(name, code)
select 'Нормализация', 'NORMALIZATION';

insert into service_group_type(name, code)
select 'Загрузка данных', 'CLUSTERIZATION';

insert into service_group_type(name, code)
select 'Визиуализация', 'VISUALISATION';

