package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vbelov.dataminingpipline.model.PipelineLinks;

import java.util.List;

@Repository
public interface PipelineLinksRepository extends JpaRepository<PipelineLinks, Long> {
    List<PipelineLinks> findAllByProjectId(Long projectId);

    void deleteAllByProjectId(Long projectId);

    void deleteAllByIdNotInAndProjectIdEquals(List<Long> ids, Long id);
}
