package ru.vbelov.dataminingpipline.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "pipeline_links")
@Data
public class PipelineLinks extends BaseObject{
    private Long projectId;
    private UUID sourceNodeUuid;
    private UUID targetNodeUuid;
}
