package ru.vbelov.dataminingpipline.dto.concatenate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StringInputDTO {

    private String input;
    private String startStr;
    private String value;
}
