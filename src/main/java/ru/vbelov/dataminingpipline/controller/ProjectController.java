package ru.vbelov.dataminingpipline.controller;

import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vbelov.dataminingpipline.dto.*;
import ru.vbelov.dataminingpipline.dto.ResponseStatus;
import ru.vbelov.dataminingpipline.model.FullProjectPipeline;
import ru.vbelov.dataminingpipline.service.PipelineExecutionService;
import ru.vbelov.dataminingpipline.service.ProjectService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("project")
@Api(description = "Функции работы с проектом анализа", tags = "Проект")
public class ProjectController {
    private final ProjectService projectService;
    private final PipelineExecutionService executionService;

    @Autowired
    public ProjectController(ProjectService projectService, PipelineExecutionService executionService) {
        this.projectService = projectService;
        this.executionService = executionService;
    }

    @PostMapping("save")
    public SuccessResponseDTO save(@RequestBody ProjectDTO projectDTO) {
        SuccessResponseDTO successResponseDTO = new SuccessResponseDTO(ResponseStatus.SUCCESS);
        projectService.saveProject(projectDTO);
        return successResponseDTO;
    }

    @GetMapping("list")
    public List<ProjectDTO> getPipelineServices() {
        return projectService.listPrjects();
    }

    @PostMapping("save/pipeline")
    public SuccessResponseDTO save(@RequestBody ProjectPipelineDTO pipelineDTO) {
        SuccessResponseDTO successResponseDTO = new SuccessResponseDTO(ResponseStatus.SUCCESS);
        projectService.savePipeline(pipelineDTO);
        return successResponseDTO;
    }

    @GetMapping("{id}/pipeline")
    public ProjectPipelineDTO getPipelineServices(@PathVariable("id") Long id) {
        return projectService.getProjectPipeline(id);
    }

    @DeleteMapping("{id}")
    public SuccessResponseDTO deletePipelineService(@PathVariable("id") Long id) {
        SuccessResponseDTO successResponseDTO = new SuccessResponseDTO(ResponseStatus.SUCCESS);
        projectService.deleteProject(id);
        return successResponseDTO;
    }

    @GetMapping("start/{projectId}/{stepUuid}")
    public JsonNode startPipeline(@PathVariable("projectId") Long projectId,
                                  @PathVariable("stepUuid") UUID stepUuid) {
        return executionService.startPiplineExecution(projectId, stepUuid);
    }

    @GetMapping("result/{stepUuid}")
    public JsonNode getPipelineStepResult(@PathVariable("stepUuid") UUID stepUuid) {
        FullProjectPipeline result = projectService.getProjecyStepResult(stepUuid);
        return result.getLastResult();
    }
}
