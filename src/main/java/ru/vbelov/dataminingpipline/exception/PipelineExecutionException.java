package ru.vbelov.dataminingpipline.exception;

public class PipelineExecutionException extends RuntimeException {

    public PipelineExecutionException(String message) {
        super(message);
    }
}
