package ru.vbelov.dataminingpipline.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class ProjectPipeline extends BaseObject{
    private UUID uuid;
    private Long projectId;
    private Long serviceId;
    @Type(type = "JsonbDataUserType")
    @Column(name = "step_metainfo")
    private JsonNode metaInfo;
}
