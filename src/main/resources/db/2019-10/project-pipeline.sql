create table if not exists project_pipeline(
  id bigserial primary key,
  name varchar (500),
  project_id integer references project(id),
  service_id integer references pipeline_service(id),
  step_metainfo jsonb,
  pipe_ord integer
);