package ru.vbelov.dataminingpipline.service;

import ru.vbelov.dataminingpipline.dto.PipelineServiceDTO;

import java.util.List;

public interface PipelineService {
    void savePipelineService(PipelineServiceDTO pipelineServiceDTO);
    List<PipelineServiceDTO> listPipelineServices();
    void deletePipelineService(Long id);
}
