package ru.vbelov.dataminingpipline.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vbelov.dataminingpipline.dto.PipelineServiceDTO;
import ru.vbelov.dataminingpipline.dto.SuccessResponseDTO;
import ru.vbelov.dataminingpipline.dto.ResponseStatus;
import ru.vbelov.dataminingpipline.service.PipelineService;

import java.util.List;

@RestController
@RequestMapping("pipelineservices")
public class PipelineServicesController {

    private final PipelineService pipelineService;

    @Autowired
    public PipelineServicesController(PipelineService pipelineService) {
        this.pipelineService = pipelineService;
    }

    @PostMapping("save")
    public SuccessResponseDTO save(@RequestBody PipelineServiceDTO pipelineServiceDTO){
        SuccessResponseDTO successResponseDTO = new SuccessResponseDTO(ResponseStatus.SUCCESS);
        pipelineService.savePipelineService(pipelineServiceDTO);
        return successResponseDTO;
    }

    @GetMapping("list")
    public List<PipelineServiceDTO> getPipelineServices(){
        return pipelineService.listPipelineServices();
    }

    @DeleteMapping("{id}")
    public SuccessResponseDTO deletePipelineService(@PathVariable("id") Long id){
        SuccessResponseDTO successResponseDTO = new SuccessResponseDTO(ResponseStatus.SUCCESS);
        pipelineService.deletePipelineService(id);
        return successResponseDTO;
    }
}
