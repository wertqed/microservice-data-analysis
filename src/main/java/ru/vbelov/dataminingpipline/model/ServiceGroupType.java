package ru.vbelov.dataminingpipline.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "service_group_type")
public class ServiceGroupType extends BaseObject {
    private String code;
}
