package ru.vbelov.dataminingpipline.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.SerializationException;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class JsonbDataUserType implements UserType, Serializable {

    private final ObjectMapper mapper = new ObjectMapper();
    private final Logger LOGGER = LoggerFactory.getLogger(JsonbDataUserType.class);

    @Override
    public int[] sqlTypes() {
        return new int[]{Types.JAVA_OBJECT};
    }

    @Nullable
    @Override
    public Object nullSafeGet(final ResultSet rs,
                              final String[] strings,
                              final SharedSessionContractImplementor session,
                              final Object obj) throws HibernateException, SQLException {
        PGobject o = (PGobject) rs.getObject(strings[0]);
        if (o != null && o.getValue() != null) {
            try {
                return mapper.readTree(o.getValue());
            } catch (IOException e) {
                LOGGER.error("Error in reading json node from string", e);
            }
        }
        return null;
    }


    @Override
    public void nullSafeSet(final PreparedStatement preparedStatement,
                            final Object value,
                            final int index,
                            final SharedSessionContractImplementor session) throws SQLException {
        if (value == null) {
            preparedStatement.setNull(index, Types.OTHER);
        } else {
            try {
                preparedStatement.setObject(index, mapper.writeValueAsString(value), Types.OTHER);
            } catch (JsonProcessingException e) {
                LOGGER.error("Error creating json string from object", e);
            }
        }
    }


    @Override
    public Object deepCopy(@Nullable final Object value) throws HibernateException {
        if (value == null) {
            return null;
        }
        try {
            return mapper.readTree(value.toString());
        } catch (IOException e) {
            LOGGER.error("Error generating deep copy", e);
            throw new RuntimeException(e);
        }
    }


    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        Object copy = deepCopy(value);

        if (copy instanceof Serializable) {
            return (Serializable) copy;
        }

        throw new SerializationException(String.format("Cannot serialize '%s', %s is not Serializable.", value, value.getClass()), null);
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return deepCopy(cached);
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(original);
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        if (x == null) {
            return 0;
        }

        return x.hashCode();
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return ObjectUtils.nullSafeEquals(x, y);
    }

    @Override
    public Class<?> returnedClass() {
        return JsonNode.class;
    }
}