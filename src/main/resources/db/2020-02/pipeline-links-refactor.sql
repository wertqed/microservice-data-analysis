drop table pipeline_links;

create table if not exists pipeline_links
  (
      id                serial primary key,
      name              character varying(250),
      project_id        integer references project (id),
      source_node_id integer references project_pipeline (id),
      target_node_id integer references project_pipeline (id)
  );