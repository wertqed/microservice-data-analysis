package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vbelov.dataminingpipline.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
}
