package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import ru.vbelov.dataminingpipline.model.DataType;
import ru.vbelov.dataminingpipline.model.ServiceGroupType;

@Repository
public interface ServiceGroupsRepository extends JpaRepository<ServiceGroupType, Long> {
}
