package ru.vbelov.dataminingpipline.dto;

public enum ResponseStatus {

    SUCCESS,
    WARNING,
    ERROR
}
