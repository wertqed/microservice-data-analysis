package ru.vbelov.dataminingpipline.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vbelov.dataminingpipline.dto.PipelineLinkDTO;
import ru.vbelov.dataminingpipline.dto.PiplineStepDTO;
import ru.vbelov.dataminingpipline.dto.ProjectDTO;
import ru.vbelov.dataminingpipline.dto.ProjectPipelineDTO;
import ru.vbelov.dataminingpipline.model.FullProjectPipeline;
import ru.vbelov.dataminingpipline.model.PipelineLinks;
import ru.vbelov.dataminingpipline.model.Project;
import ru.vbelov.dataminingpipline.model.ProjectPipeline;
import ru.vbelov.dataminingpipline.repository.FullProjectPipelineRepository;
import ru.vbelov.dataminingpipline.repository.PipelineLinksRepository;
import ru.vbelov.dataminingpipline.repository.ProjectPipelineRepository;
import ru.vbelov.dataminingpipline.repository.ProjectRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);

    private final ProjectRepository projectRepository;
    private final ProjectPipelineRepository projectPipelineRepository;
    private final PipelineLinksRepository pipelineLinksRepository;
    private final FullProjectPipelineRepository fullProjectPipelineRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, ProjectPipelineRepository projectPipelineRepository, PipelineLinksRepository pipelineLinksRepository, FullProjectPipelineRepository fullProjectPipelineRepository) {
        this.projectRepository = projectRepository;
        this.projectPipelineRepository = projectPipelineRepository;
        this.pipelineLinksRepository = pipelineLinksRepository;
        this.fullProjectPipelineRepository = fullProjectPipelineRepository;
    }

    @Override
    public void saveProject(ProjectDTO projectDTO) {
        LOGGER.debug("Saving new project = {}", projectDTO);
        Project entity = new Project();
        BeanUtils.copyProperties(projectDTO, entity);
        projectRepository.save(entity);
    }

    @Override
    public List<ProjectDTO> listPrjects() {
        LOGGER.debug("Loading pipelineservices list");
        List<ProjectDTO> projectDTOS = new ArrayList<>();
        projectRepository.findAll().forEach(entity -> {
            ProjectDTO dto = new ProjectDTO();
            BeanUtils.copyProperties(entity, dto);
            projectDTOS.add(dto);
        });
        return projectDTOS;
    }

    @Override
    @Transactional
    public void deleteProject(Long id) {
        LOGGER.debug("Delete project by id ={}", id);
        pipelineLinksRepository.deleteAllByProjectId(id);
        projectPipelineRepository.deleteAllByProjectId(id);
        projectRepository.deleteById(id);
    }

    @Override
    public ProjectPipelineDTO getProjectPipeline(Long projectId) {
        ProjectPipelineDTO dto = new ProjectPipelineDTO();
        dto.setProjectId(projectId);
        dto.setPipelineSteps(projectPipelineRepository.findProjectPipelineByProjectId(projectId).stream()
                .map(entity -> {
                    PiplineStepDTO stepDTO = new PiplineStepDTO();
                    BeanUtils.copyProperties(entity, stepDTO);
                    return stepDTO;
                })
                .collect(Collectors.toList()));
        dto.setPipelineLinks(pipelineLinksRepository.findAllByProjectId(projectId).stream()
                .map(link -> {
                    PipelineLinkDTO linkDTO = new PipelineLinkDTO();
                    BeanUtils.copyProperties(link, linkDTO);
                    return linkDTO;
                })
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    @Transactional
    public void savePipeline(ProjectPipelineDTO pipelineDTO) {
        // Сначала вычищаем сущности которых нет в списке
        projectPipelineRepository.deleteAllByIdNotInAndProjectIdEquals(pipelineDTO.getPipelineSteps().stream()
                        .map(PiplineStepDTO::getId)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()),
                pipelineDTO.getProjectId());
        // Затем сохраняем список
        projectPipelineRepository.saveAll(pipelineDTO.getPipelineSteps().stream()
                .map(dto -> {
                    ProjectPipeline entity = new ProjectPipeline();
                    BeanUtils.copyProperties(dto, entity);
                    entity.setProjectId(pipelineDTO.getProjectId());
                    return entity;
                })
                .collect(Collectors.toList()));

        pipelineLinksRepository.deleteAllByIdNotInAndProjectIdEquals(pipelineDTO.getPipelineLinks().stream()
                        .map(PipelineLinkDTO::getId)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()),
                pipelineDTO.getProjectId());
        pipelineLinksRepository.saveAll(pipelineDTO.getPipelineLinks().stream()
                .map(dto -> {
                    PipelineLinks entity = new PipelineLinks();
                    BeanUtils.copyProperties(dto, entity);
                    entity.setProjectId(pipelineDTO.getProjectId());
                    return entity;
                })
                .collect(Collectors.toList()));
    }

    @Override
    public FullProjectPipeline getProjecyStepResult(UUID stepUuid) {
        return fullProjectPipelineRepository.findFirstByUuid(stepUuid);
    }
}
