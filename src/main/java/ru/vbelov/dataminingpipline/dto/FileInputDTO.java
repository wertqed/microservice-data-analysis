package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

@Data
public class FileInputDTO {
    private String file;
    private String delimeter;
    private Integer limit;
    private Boolean useHeader;
}
