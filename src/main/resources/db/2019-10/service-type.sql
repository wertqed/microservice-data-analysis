create table if not exists pipeline_service_type(
id integer primary key,
name character varying(250)
);

insert into pipeline_service_type
values (1, 'Загрузчик данных');

alter table pipeline_service
add column type_id integer references pipeline_service_type(id);