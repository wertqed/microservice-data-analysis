package ru.vbelov.dataminingpipline.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vbelov.dataminingpipline.dto.DictTypeDTO;
import ru.vbelov.dataminingpipline.repository.DataTypeRepository;
import ru.vbelov.dataminingpipline.repository.ServiceGroupsRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("dict")
public class DictionaryController {

    private final DataTypeRepository dataTypeRepository;
    private final ServiceGroupsRepository serviceGroupsRepository;

    public DictionaryController(DataTypeRepository dataTypeRepository, ServiceGroupsRepository serviceGroupsRepository) {
        this.dataTypeRepository = dataTypeRepository;
        this.serviceGroupsRepository = serviceGroupsRepository;
    }

    @GetMapping("datatype/list")
    public List<DictTypeDTO> listDataType() {
        return dataTypeRepository.findAll().stream()
                .map(entity -> {
                    DictTypeDTO dto = new DictTypeDTO();
                    BeanUtils.copyProperties(entity, dto);
                    return dto;
                }).collect(Collectors.toList());
    }

    @GetMapping("servicegroups/list")
    public List<DictTypeDTO> listServiceGroups() {
        return serviceGroupsRepository.findAll().stream()
                .map(entity -> {
                    DictTypeDTO dto = new DictTypeDTO();
                    BeanUtils.copyProperties(entity, dto);
                    return dto;
                }).collect(Collectors.toList());
    }
}
