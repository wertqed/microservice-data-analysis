package ru.vbelov.dataminingpipline.dto.transform;

import lombok.Data;

import java.util.List;

@Data
public class KeyArrayInputDTO {
    private List<List<Object>> input;
    private Integer keyRowIndex;
}
