package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

import java.util.List;

@Data
public class FileOutputDTO {
    private List<List<Object>> output;
}
