package ru.vbelov.dataminingpipline.dto.transform;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class KeyArrayOutputDTO {
    private Map<Object, List<Object>> input;
}
