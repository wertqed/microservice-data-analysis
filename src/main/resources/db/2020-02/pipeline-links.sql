create table if not exists pipeline_links
  (
      id                serial primary key,
      name              character varying(250),
      project_id        integer references project (id),
      source_service_id integer references pipeline_service (id),
      target_service_id integer references pipeline_service (id)
  );