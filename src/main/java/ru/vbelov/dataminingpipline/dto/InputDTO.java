package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

@Data
public class InputDTO {
    private String input;
}
