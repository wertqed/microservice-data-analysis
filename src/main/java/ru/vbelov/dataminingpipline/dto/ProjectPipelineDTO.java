package ru.vbelov.dataminingpipline.dto;

import lombok.Data;
import ru.vbelov.dataminingpipline.model.PipelineLinks;

import java.util.List;

@Data
public class ProjectPipelineDTO {
    private Long projectId;
    private List<PiplineStepDTO> pipelineSteps;
    private List<PipelineLinkDTO> pipelineLinks;
}
