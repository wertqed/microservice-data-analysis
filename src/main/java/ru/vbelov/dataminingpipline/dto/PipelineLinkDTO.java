package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PipelineLinkDTO {
    private Long id;
    private String name;
    private UUID sourceNodeUuid;
    private UUID targetNodeUuid;
}
