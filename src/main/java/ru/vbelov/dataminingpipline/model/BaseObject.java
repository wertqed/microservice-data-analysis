package ru.vbelov.dataminingpipline.model;

import lombok.Data;
import org.hibernate.annotations.TypeDef;
import ru.vbelov.dataminingpipline.config.JsonbDataUserType;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
@TypeDef(name = "JsonbDataUserType", typeClass = JsonbDataUserType.class)
public class BaseObject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
}
