package ru.vbelov.dataminingpipline.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "pipeline_service")
@Data
public class PipelineServiceEntity extends BaseObject{
    private String groupCode;
    private String port;
    private String host;
    private String path;
    private String inDataTypeCode;
    private String outDataTypeCode;
    @Type(type = "JsonbDataUserType")
    private JsonNode metaInfo;
}
