package ru.vbelov.dataminingpipline.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.vbelov.dataminingpipline.dto.PipelineLinkDTO;
import ru.vbelov.dataminingpipline.dto.PiplineStepDTO;
import ru.vbelov.dataminingpipline.dto.ProjectPipelineDTO;
import ru.vbelov.dataminingpipline.exception.PipelineExecutionException;
import ru.vbelov.dataminingpipline.model.FullProjectPipeline;
import ru.vbelov.dataminingpipline.model.PipelineServiceEntity;
import ru.vbelov.dataminingpipline.repository.FullProjectPipelineRepository;
import ru.vbelov.dataminingpipline.repository.PipelineServicesRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class PipelineExecutionServiceImpl implements PipelineExecutionService {

    private final ProjectService projectService;
    private final PipelineServicesRepository servicesRepository;
    private final ObjectMapper objectMapper;
    private final FullProjectPipelineRepository projectPipelineRepository;

    public PipelineExecutionServiceImpl(ProjectService projectService, PipelineServicesRepository servicesRepository,
                                        ObjectMapper objectMapper, FullProjectPipelineRepository projectPipelineRepository) {
        this.projectService = projectService;
        this.servicesRepository = servicesRepository;
        this.objectMapper = objectMapper;
        this.projectPipelineRepository = projectPipelineRepository;
    }

    @Override
    public JsonNode startPiplineExecution(Long projectId, UUID stepUuid) {
        ProjectPipelineDTO projectPipelineDTO = projectService.getProjectPipeline(projectId);
        List<PiplineStepDTO> pipeline = buildPipeline(new ArrayList<>(), stepUuid, projectPipelineDTO);
        Collections.reverse(pipeline);
        JsonNode out = null;
        for (PiplineStepDTO step : pipeline) {
            ObjectNode input = objectMapper.createObjectNode();
            step.getMetaInfo().forEach(jsonNode -> {
                String fieldName = jsonNode.get("name").asText();
                JsonNode fieldValue = jsonNode.get("value");
                if (fieldValue.isTextual()) {
                    input.put(fieldName, fieldValue.asText());
                } else if (fieldValue.isDouble()) {
                    input.put(fieldName, fieldValue.asDouble());
                } else if (fieldValue.isInt()) {
                    input.put(fieldName, fieldValue.asInt());
                } else if (fieldValue.isLong()) {
                    input.put(fieldName, fieldValue.asLong());
                } else if (fieldValue.isBoolean()) {
                    input.put(fieldName, fieldValue.asBoolean());
                } else {
                    input.put(fieldName, fieldValue);
                }
            });
            if (out != null) {
                input.put("input", out.get("output"));
            }
            PipelineServiceEntity serviceEntity = servicesRepository.getById(step.getServiceId());
            out = sendReq(serviceEntity, input);
            // Сохранение результата каждого шага
            FullProjectPipeline stepEntity = projectPipelineRepository.findFirstByUuid(step.getUuid());
            stepEntity.setLastResult(out);
            projectPipelineRepository.save(stepEntity);
        }
        return out;
    }

    private List<PiplineStepDTO> buildPipeline(List<PiplineStepDTO> result, UUID stepUuid, ProjectPipelineDTO projectPipelineDTO) {
        if (stepUuid == null) {
            return result;
        }
        result.add(projectPipelineDTO.getPipelineSteps().stream()
                .filter(step -> step.getUuid().equals(stepUuid))
                .findFirst()
                .orElseThrow(() -> new PipelineExecutionException("Ошибка построения цепочки анализа, не найден элемент с uuid: " + stepUuid)));
        PipelineLinkDTO linkDTO = projectPipelineDTO.getPipelineLinks().stream()
                .filter(link -> link.getTargetNodeUuid().equals(stepUuid))
                .findFirst()
                .orElse(null);
        return buildPipeline(result, linkDTO != null ? linkDTO.getSourceNodeUuid() : null, projectPipelineDTO);
    }

    private JsonNode sendReq(PipelineServiceEntity serviceEntity, JsonNode in) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<JsonNode> entity = new HttpEntity<JsonNode>(in, headers);
        ResponseEntity<JsonNode> response = restTemplate.postForEntity(serviceEntity.getHost() + ":" +
                serviceEntity.getPort() + serviceEntity.getPath(), entity, JsonNode.class);
        return response.getBody();
    }
}
