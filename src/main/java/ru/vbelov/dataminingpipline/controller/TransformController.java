package ru.vbelov.dataminingpipline.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.vbelov.dataminingpipline.dto.transform.KeyArrayInputDTO;
import ru.vbelov.dataminingpipline.dto.transform.KeyArrayOutputDTO;

import java.util.List;

@Controller
@RequestMapping("transform")
public class TransformController {

    @PostMapping("key-array")
    public KeyArrayOutputDTO transformKeyArray(@RequestBody KeyArrayInputDTO keyArrayInputDTO) {
        List<List<Object>> input = keyArrayInputDTO.getInput();
        KeyArrayOutputDTO outputDTO = new KeyArrayOutputDTO();
        return outputDTO;
//        List<> = input.get(keyArrayInputDTO.getKeyRowIndex());
    }
}
