package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;
import ru.vbelov.dataminingpipline.model.FullProjectPipeline;

import java.util.UUID;

@Repository
public interface FullProjectPipelineRepository extends JpaRepository<FullProjectPipeline, Long> {
    FullProjectPipeline findFirstByUuid(UUID uuid);
}
