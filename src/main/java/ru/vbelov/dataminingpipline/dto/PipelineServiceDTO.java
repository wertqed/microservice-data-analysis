package ru.vbelov.dataminingpipline.dto;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class PipelineServiceDTO {
    private Long id;
    private String name;
    private String groupCode;
    private String port;
    private String host;
    private String path;
    private String inDataTypeCode;
    private String outDataTypeCode;
    private JsonNode metaInfo;
}
