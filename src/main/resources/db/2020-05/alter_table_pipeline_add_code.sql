alter table pipeline_service
    add column in_data_type_code character varying(100);

alter table pipeline_service
    add column out_data_type_code character varying(100);