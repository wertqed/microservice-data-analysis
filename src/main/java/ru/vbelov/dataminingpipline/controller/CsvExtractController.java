package ru.vbelov.dataminingpipline.controller;

import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vbelov.dataminingpipline.dto.FileInputDTO;
import ru.vbelov.dataminingpipline.dto.FileOutputDTO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@RestController
@RequestMapping("extract")
public class CsvExtractController {

    @PostMapping("csv")
    public FileOutputDTO getCsvData(@RequestBody FileInputDTO inputDTO) throws IOException {
        File file = new File("path");
        byte[] bytes = Base64.decodeBase64(inputDTO.getFile());
        FileUtils.writeByteArrayToFile(file, bytes);
        List<List<Object>> records = new ArrayList<>();
        int cnt = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = "";
            if (inputDTO.getUseHeader() != null && inputDTO.getUseHeader()) {
                br.readLine();
            }
            while ((line = br.readLine()) != null && (inputDTO.getLimit() == null || cnt < inputDTO.getLimit())) {
                records.add(getRecordFromLine(line, inputDTO.getDelimeter()));
                cnt++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        try (Scanner scanner = new Scanner(file, "UTF-8")) {
//            while (scanner.hasNextLine()) {
//                records.add(getRecordFromLine(scanner.nextLine(), inputDTO.getDelimeter()));
//            }
//        }
        FileOutputDTO fileOutputDTO = new FileOutputDTO();
        fileOutputDTO.setOutput(records);
        return fileOutputDTO;
    }

    private List<Object> getRecordFromLine(String line, String delimeter) {
        List<Object> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(delimeter);
            while (rowScanner.hasNext()) {
                if (rowScanner.hasNextDouble()) {
                    values.add(rowScanner.nextDouble());
                } else if (rowScanner.hasNextLong()) {
                    values.add(rowScanner.nextLong());
                } else if (rowScanner.hasNextInt()) {
                    values.add(rowScanner.nextInt());
                } else if (rowScanner.hasNextBoolean()) {
                    values.add(rowScanner.nextBoolean());
                } else {
                    values.add(rowScanner.next());
                }
            }
        }
        return values;
    }
}
