insert into data_type(name, code)
select 'Двумерный массив', 'TWO_DIM_ARRAY';

insert into data_type(name, code)
select 'Список', 'LIST';

insert into data_type(name, code)
select 'Словарь', 'DICTIONARY';

insert into data_type(name, code)
select 'Ключ->Список', 'KEY-ARRAY';

insert into data_type(name, code)
select 'Файл', 'FILE';