package ru.vbelov.dataminingpipline.dto;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

import java.util.UUID;

@Data
public class PiplineStepDTO {
    private Long id;
    private UUID uuid;
    private String name;
    private Long serviceId;
    private JsonNode metaInfo;
}
