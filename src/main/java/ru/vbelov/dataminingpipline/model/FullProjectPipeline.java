package ru.vbelov.dataminingpipline.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "project_pipeline")
public class FullProjectPipeline extends BaseObject {
    private UUID uuid;
    private Long projectId;
    private Long serviceId;
    @Type(type = "JsonbDataUserType")
    @Column(name = "step_metainfo")
    private JsonNode metaInfo;
    @Type(type = "JsonbDataUserType")
    @Column(name = "last_result")
    private JsonNode lastResult;
}
