package ru.vbelov.dataminingpipline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vbelov.dataminingpipline.model.PipelineServiceEntity;

@Repository
public interface PipelineServicesRepository extends JpaRepository<PipelineServiceEntity, Long> {
    PipelineServiceEntity getById(Long id);
}
