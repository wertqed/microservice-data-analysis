package ru.vbelov.dataminingpipline.service;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.UUID;

public interface PipelineExecutionService {

    JsonNode startPiplineExecution(Long projectId, UUID stepUuid);
}
