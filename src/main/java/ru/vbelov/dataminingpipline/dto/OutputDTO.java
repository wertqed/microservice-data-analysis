package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

@Data
public class OutputDTO {
    private String output;
}
