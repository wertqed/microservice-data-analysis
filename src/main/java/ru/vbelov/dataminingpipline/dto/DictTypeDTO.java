package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

@Data
public class DictTypeDTO {
    private Long id;
    private String code;
    private String name;
}
