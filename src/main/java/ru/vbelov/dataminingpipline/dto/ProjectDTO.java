package ru.vbelov.dataminingpipline.dto;

import lombok.Data;

@Data
public class ProjectDTO {
    private Long id;
    private String name;
}
