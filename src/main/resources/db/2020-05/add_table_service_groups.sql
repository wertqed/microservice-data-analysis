create table if not exists service_group_type
(
    id   serial primary key,
    name character varying(250),
    code character varying(100)
);