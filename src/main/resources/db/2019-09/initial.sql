create table if not exists data_type(
  id serial primary key,
  name character varying(256)
);

create table if not exists pipeline_service(
  id serial primary key,
  name character varying(512),
  host character varying(512),
  port character varying (10),
  in_data_type_id integer,
  out_data_type_id integer
);